var WebSqlDB = function(successCallback, errorCallback) {

    this.initializeDatabase = function(successCallback, errorCallback) {

        // This here refers to this instance of the webSqlDb
        var self = this;

        // Open/create the database
        this.db = window.openDatabase("churrascodb", "1.0", "Churrasco Database", 4 * 1014 * 1024);   
        
        // WebSQL databases are tranaction based so all db querying must be done within a transaction
        this.db.transaction(
                function(tx) {
                    self.createTable(tx);
                    self.addSampleData(tx);
                },
                function(error) {
                    console.log('Transaction error: ' + error);
                    if (errorCallback) errorCallback();
                },
                function() {
                    console.log('DEBUG - 5. initializeDatabase complete');
                    if (successCallback) successCallback();
                }
        )
    }

    this.createTable = function(tx) {
        var sqlTabelaPercentAlimento = 
            " create table if not exists percentalimento ( " +
            "     nomealimento varchar(60) primary key not null, " +
            "     percentual integer not null " +
            " ) ";
        var sqlPercentBebida =
            " create table if not exists percentbebida ( " +
            "     tipobebida varchar(60) primary key not null, " +
            "     percentual integer not null " +
            " ) ";
        var sqlParticipantes =
            " create table if not exists participantes ( " +
            "     tipopessoa varchar(60) primary key not null, " +
            "     quantidade integer not null " +
            " ); ";
        var sqlPesoPessoa =
            " create table if not exists pesopessoa ( " +
            "     tipopessoa varchar(60) primary key not null, " +
            "     peso integer not null " +
            " ); ";
        
        tx.executeSql(sqlTabelaPercentAlimento, null,
                function() {            // Success callback
                    console.log('DEBUG - 1. DB Tables sqlTabelaPercentAlimento created succesfully');
                },
                function(tx, error) {   // Error callback
                    alert('Create table sqlTabelaPercentAlimento error: ' + error.message);
                });
        tx.executeSql(sqlPercentBebida, null,
                function() {            // Success callback
                    console.log('DEBUG - 2. DB Tables sqlPercentBebida created succesfully');
                },
                function(tx, error) {   // Error callback
                    alert('Create table sqlPercentBebida error: ' + error.message);
                });
        tx.executeSql(sqlParticipantes, null,
                function() {            // Success callback
                    console.log('DEBUG - 3. DB Tables sqlParticipantes created succesfully');
                },
                function(tx, error) {   // Error callback
                    alert('Create table sqlParticipantes error: ' + error.message);
                });
        tx.executeSql(sqlPesoPessoa, null,
                function() {            // Success callback
                    console.log('DEBUG - 4. DB Tables sqlPesoPessoa created succesfully');
                },
                function(tx, error) {   // Error callback
                    alert('Create table sqlPesoPessoa error: ' + error.message);
                });
    }

    this.addSampleData = function(tx) {
        // Array of objects
        var alimentos = [
                {"nomealimento": "Carne", "percentual": 90},
                {"nomealimento": "Salada", "percentual": 10}
            ];
        
        // Array of objects
        var bebidas = [
                {"tipobebida": "Cerveja", "percentual": 80},
                {"tipobebida": "Refrigerante", "percentual": 20}
            ];
        
        var pessoas = [
                {"tipopessoa": "Homem", "quantidade": 10},
                {"tipopessoa": "Mulher", "quantidade": 10},
                {"tipopessoa": "Criança", "quantidade": 6}
            ];
        
        var pesopessoas = [
                {"tipopessoa": "Homem", "peso": 350},
                {"tipopessoa": "Mulher", "peso": 280},
                {"tipopessoa": "Criança", "peso": 200}
            ];
        
        var ArrayAlimentos = alimentos.length;
        var ArrayBebidas = bebidas.length;
        var ArrayPessoas = pessoas.length;
        var ArrayPesoPessoas = pesopessoas.length;
        

        var sqlInsAlimento = "INSERT OR REPLACE INTO percentalimento " +
            " (nomealimento, percentual) " +
            " VALUES (?, ?)";
        
        var sqlInsBebida = "INSERT OR REPLACE INTO percentbebida " +
            " (tipobebida, percentual) " +
            " VALUES (?, ?)";
        
        var sqlInsParticipante = "INSERT OR REPLACE INTO participantes " +
            " (tipopessoa, quantidade) " +
            " VALUES (?, ?)";
        
        var sqlInsPesoP = "INSERT OR REPLACE INTO pesopessoa " +
            " (tipopessoa, peso) " +
            " VALUES (?, ?)";
              
        // Loop through sample data array and insert into db
       
        for (var i = 0; i < ArrayAlimentos; i++) {
           
            a = alimentos[i];
            
            tx.executeSql(sqlInsAlimento, [a.nomealimento, a.percentual],
                    function() {            // Success callback
                        console.log('DEBUG - 5. Sample alimento data DB insert success: ' + a.nomealimento +  a.percentual);
                    },
                    function(tx, error) {   // Error callback
                        alert('INSERT alimento error: ' + error.message);
                    });
        }
        
        for (var i = 0; i < ArrayBebidas; i++) {
            b = bebidas[i];
            tx.executeSql(sqlInsBebida, [b.tipobebida, b.percentual],
                    function() {            // Success callback
                        console.log('DEBUG - 4. Sample Bebida data DB insert success');
                    },
                    function(tx, error) {   // Error callback
                        alert('INSERT bebida error: ' + error.message);
                    });
        }
        
        for (var i = 0; i < ArrayPessoas; i++) {
            p = pessoas[i];
            tx.executeSql(sqlInsParticipante, [p.tipopessoa, p.quantidade],
                    function() {            // Success callback
                        console.log('DEBUG - 4. Sample Participante data DB insert success');
                    },
                    function(tx, error) {   // Error callback
                        alert('INSERT Participante error: ' + error.message);
                    });
        }
        
        for (var i = 0; i < ArrayPesoPessoas; i++) {
            pp = pesopessoas[i];
            tx.executeSql(sqlInsPesoP, [pp.tipopessoa, pp.peso],
                    function() {            // Success callback
                        console.log('DEBUG - 4. Sample Peso Pessoa data DB insert success');
                    },
                    function(tx, error) {   // Error callback
                        alert('INSERT Peso Pessoa error: ' + error.message);
                    });
        }
    }

    this.findAlimentosAll = function(callback) {  
        
        this.db.transaction(
            function(tx) {
                var sql = "SELECT * FROM percentalimento";
                tx.executeSql(sql, null, function(tx, results) {
                    var len = results.rows.length, alimentos = [], i = 0;
                     for (; i < len; i++) {
                        alimentos[i] = results.rows.item(i);
                    }
                    
                    // Passes a array with values back to calling function
                    callback(alimentos);
                });
            },
            function(tx, error) {
                alert("Transaction Error findAll alimentos: " + error);
            }
        );
    }

    this.findAlimentoByNome = function(nomealimento, callback) {        
        this.db.transaction(
            function(tx) {
                var sql = "SELECT * FROM percentalimento WHERE nomealimento=?";
                tx.executeSql(sql, [nomealimento], function(tx, results) {
                    // This callback returns the first results.rows.item if rows.length is 1 or return null
                    callback(results.rows.length === 1 ? results.rows.item(0) : null);
                });
            },
            function(tx, error) {
                alert("Transaction Error findAlimentoByNome: " + error.message);
            }
        );
    }

    this.updateAlimento = function(json, callback) {
        // Converts a JavaScript Object Notation (JSON) string into an object.
        var parsedJson = JSON.parse(json);
              
        this.db.transaction(
            function (tx) {
                var sql = "UPDATE percentalimento SET percentual=? WHERE nomealimento=?";
                tx.executeSql(sql, [parsedJson.percentual, parsedJson.nomealimento], function(tx, result) {
                    // If results rows
                    callback(result.rowsAffected === 1 ? true : false);
                });
            }
        );
    }

    this.findBebidaAll = function(callback) {        
        this.db.transaction(
            function(tx) {
                var sql = "SELECT * FROM percentbebida";
                tx.executeSql(sql, [], function(tx, results) {
                    var len = results.rows.length,
                        bebidas = [],
                        i = 0;

                    // Semicolon at the start is to skip the initialisation of vars as we already initalise i above.
                    for (; i < len; i = i + 1) {
                        bebidas[i] = results.rows.item(i);
                    }

                    // Passes a array with values back to calling function
                    callback(bebidas);
                });
            },
            function(error) {
                alert("Transaction Error bebidas findAll: " + error.message);
            }
        );
    }

    this.findBedidaByNome = function(tipobebida, callback) {        
        this.db.transaction(
            function(tx) {
                var sql = "SELECT * FROM aluno WHERE tipobebida=?";
                tx.executeSql(sql, [tipobebida], function(tx, results) {
                    // This callback returns the first results.rows.item if rows.length is 1 or return null
                    callback(results.rows.length === 1 ? results.rows.item(0) : null);
                });
            },
            function(error) {
                alert("Transaction findBedidaByNome Error: " + error.message);
            }
        );
    }

    this.findParticipantesAll = function(callback) {        
        this.db.transaction(
            function(tx) {
                var sql = "SELECT * FROM participantes";
                tx.executeSql(sql, [], function(tx, results) {
                    var len = results.rows.length,
                        parti = [],
                        i = 0;

                    // Semicolon at the start is to skip the initialisation of vars as we already initalise i above.
                    for (; i < len; i = i + 1) {
                        parti[i] = results.rows.item(i);
                    }

                    // Passes a array with values back to calling function
                    callback(parti);
                });
            },
            function(error) {
                alert("Transaction Error participantes findAll: " + error.message);
            }
        );
    }
    
    this.updateParticipante = function(json, callback) {
        var parsedJson = JSON.parse(json);
              
        this.db.transaction(
            function (tx) {
                var sql = "UPDATE participantes SET quantidade=? WHERE tipopessoa=?";
                tx.executeSql(sql, [parsedJson.quantidade, parsedJson.tipopessoa], function(tx, result) {
                    // If results rows
                    callback(result.rowsAffected === 1 ? true : false);
                });
            }
        );
    }
    
    this.findPesoPessoaAll = function(callback) {        
        this.db.transaction(
            function(tx) {
                var sql = "SELECT * FROM pesopessoa";
                tx.executeSql(sql, [], function(tx, results) {
                    var len = results.rows.length,
                        pesopessoa = [],
                        i = 0;

                    // Semicolon at the start is to skip the initialisation of vars as we already initalise i above.
                    for (; i < len; i = i + 1) {
                        pesopessoa[i] = results.rows.item(i);
                    }

                    // Passes a array with values back to calling function
                    callback(pesopessoa);
                });
            },
            function(error) {
                alert("Transaction Error pesopessoa findAll: " + error.message);
            }
        );
    }
    
    this.updatePesoPessoa = function(json, callback) {
        var parsedJson = JSON.parse(json);
              
        this.db.transaction(
            function (tx) {
                var sql = "UPDATE pesopessoa SET peso=? WHERE tipopessoa=?";
                tx.executeSql(sql, [parsedJson.peso, parsedJson.tipopessoa], function(tx, result) {
                    // If results rows
                    callback(result.rowsAffected === 1 ? true : false);
                });
            }
        );
    }
    
    // inicializando base de dados
    this.initializeDatabase(successCallback, errorCallback);
}