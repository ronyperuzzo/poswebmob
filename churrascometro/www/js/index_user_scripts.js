var db = new WebSqlDB(sucesso, erro);

function sucesso() {
    console.log("sucesso DV");
}

function erro(error) {
    console.log("Erro de DB: " + error);
}

var padAssinatura;

/*jshint browser:true */
/*global $ */
(function () {
    "use strict";
    /*
      hook up event handlers 
    */
    function register_event_handlers() {

        

        /* button  #btnmenu */
        $(document).on("click", "#btnmenu", function (evt) {
            /*global uib_sb */
            /* Other possible functions are: 
              uib_sb.open_sidebar($sb)
              uib_sb.close_sidebar($sb)
              uib_sb.toggle_sidebar($sb)
               uib_sb.close_all_sidebars()
             See js/sidebar.js for the full sidebar API */

            uib_sb.toggle_sidebar($("#sbmenu"));
            return false;
        });

                
        /* button  #btnAlimentos */
    $(document).on("click", "#btnAlimentos", function(evt)
    {
        db.findAlimentosAll(function (alimentos) {
                // limpando a lista
            for (var i = 0; i < alimentos.length; i++) {
                if(alimentos[i].nomealimento == 'Salada') {
                    $("#inputSalada").val(alimentos[i].percentual);
                }
                
                if(alimentos[i].nomealimento == 'Carne') {
                   $("#inputCarne").val(alimentos[i].percentual);
                }    
                    
                
                
                }
            });
        
        activate_subpage("#alimentos");
        uib_sb.toggle_sidebar($("#sbmenu"));
        return false;
    });
    
    
    $(document).on("click", "#salvarAlimento", function(evt)
    {
        
        db.updateAlimento(JSON.stringify({
                 "percentual": $("#inputSalada").val(),
                 "nomealimento": "Salada"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
        db.updateAlimento(JSON.stringify({
                 "percentual": $("#inputCarne").val(),
                 "nomealimento": "Carne"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
        return false;
    });
    
        
    $(document).on("click", "#btnMenuBebidas", function(evt)
    {
        db.findBebidaAll(function (bebidas) {
               
            for (var i = 0; i < bebidas.length; i++) {
                if(bebidas[i].tipobebida == 'Cerveja') {
                    $("#inputCerveja").val(bebidas[i].percentual);
                }
                
                if(bebidas[i].tipobebida == 'Refrigerante') {
                   $("#inputRefrigerante").val(bebidas[i].percentual);
                }    
                    
                
                
            }
        });
        
        activate_subpage("#bebidas");
        uib_sb.toggle_sidebar($("#sbmenu"));
        return false;
    });
    
       
    $(document).on("click", "#btnVoltarAlimentos", function(evt)
    {
        /* your code goes here */ 
        return false;
    });
    

        /* button  #btnMenuParticipantes */
    $(document).on("click", "#btnMenuParticipantes", function(evt)
    {
         db.findParticipantesAll(function (part) {
               
            for (var i = 0; i < part.length; i++) {
                if(part[i].tipopessoa == 'Homem') {
                    $("#inputHomem").val(part[i].quantidade);
                }
                
                if(part[i].tipopessoa == 'Mulher') {
                   $("#inputMulher").val(part[i].quantidade);
                }   
                
                if(part[i].tipopessoa == 'Criança') {
                   $("#inputCrianca").val(part[i].quantidade);
                }  
                        
            }
        });
        
        
        activate_subpage("#participantes");
        uib_sb.toggle_sidebar($("#sbmenu"));
        return false;
    });
        
        
    $(document).on("click", "#btnSalvarParticipantes", function(evt)
    {
        db.updateParticipante(JSON.stringify({
                 "quantidade": $("#inputHomem").val(),
                 "tipopessoa": "Homem"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
         db.updateParticipante(JSON.stringify({
                 "quantidade": $("#inputMulher").val(),
                 "tipopessoa": "Mulher"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
    
         db.updateParticipante(JSON.stringify({
                 "quantidade": $("#inputCrianca").val(),
                 "tipopessoa": "Criança"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
         return false;
    
    });
               
    
    /* button  #btnMenuPesoPessoa */
    $(document).on("click", "#btnMenuPesoPessoa", function(evt)
    {
        db.findPesoPessoaAll(function (part) {
            
           for (var i = 0; i < part.length; i++) {
                                            
                if(part[i].tipopessoa == 'Homem') {
                    $("#inputPesoHomem").val(part[i].peso);
                }
                
                if(part[i].tipopessoa == 'Mulher') {
                   $("#inputPesoMulher").val(part[i].peso);
                }   
                
                if(part[i].tipopessoa == 'Criança') {
                   $("#inputPesoCrianca").val(part[i].peso);
                }  
                        
            }
        });
        
        
        activate_subpage("#pesopessoa");
        uib_sb.toggle_sidebar($("#sbmenu"));
        return false;
    });
    
        /* button  #btnSalvarPesoPessoa */
    $(document).on("click", "#btnSalvarPesoPessoa", function(evt)
    {
        db.updatePesoPessoa(JSON.stringify({
                 "peso": $("#inputPesoHomem").val(),
                 "tipopessoa": "Homem"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
         db.updatePesoPessoa(JSON.stringify({
                 "quantidade": $("#inputPesoMulher").val(),
                 "tipopessoa": "Mulher"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
    
         db.updatePesoPessoa(JSON.stringify({
                 "quantidade": $("#inputPesoCrianca").val(),
                 "tipopessoa": "Criança"
                }), function (status) {
                    if (status == true) {
                        // capturando os dados do aluno da tela        
                        navigator.notification.alert("Atualizado com sucesso!");
                    }
                });
         return false;
    });
    
    var percentCarne;
    var percentSalada;
        
    var percentCerveja;
    var percentRefri;
        
    var qtdHomem;
    var qtdMulher;
    var qtdCrianca;
        
    var pesoMedioHomem;
    var pesoMedioMulher;
    var pesoMedioCrianca;    
    
    $(document).on("click", "#btnMenuCalcular", function(evt)
    {
        
        
        db.findAlimentosAll(function (alimentos) {
            for (var i = 0; i < alimentos.length; i++) {
                if(alimentos[i].nomealimento == 'Salada') {
                   percentSalada = alimentos[i].percentual;
                }
                
                if(alimentos[i].nomealimento == 'Carne') {
                    percentCarne = alimentos[i].percentual;
                }    
            }
        });
               
        db.findBebidaAll(function (bebidas) {
               
            for (var i = 0; i < bebidas.length; i++) {
                if(bebidas[i].tipobebida == 'Cerveja') {
                    percentCerveja = bebidas[i].percentual;
                }
                
                if(bebidas[i].tipobebida == 'Refrigerante') {
                    percentRefri = bebidas[i].percentual;
                }    
            }
        });
                
        
        db.findParticipantesAll(function (part) {      
            for (var i = 0; i < part.length; i++) {
                if(part[i].tipopessoa == 'Homem') {
                    qtdHomem = part[i].quantidade;
                }
                
                if(part[i].tipopessoa == 'Mulher') {
                   qtdMulher = part[i].quantidade;
                }   
                
                if(part[i].tipopessoa == 'Criança') {
                   qtdCrianca = part[i].quantidade;
                }  
            }
        });
        
       
        db.findPesoPessoaAll(function (part) {
            
           for (var i = 0; i < part.length; i++) {
                                            
                if(part[i].tipopessoa == 'Homem') {
                    pesoMedioHomem = part[i].peso;
                }
                
                if(part[i].tipopessoa == 'Mulher') {
                   pesoMedioMulher = part[i].peso;
                }   
                
                if(part[i].tipopessoa == 'Criança') {
                   pesoMedioCrianca = part[i].peso;
                }  
            }
        });
        
        /*global activate_subpage */
        activate_subpage("#calcular"); 
            
        uib_sb.toggle_sidebar($("#sbmenu")); 
        return false;
    });
    
        /* button  #btnCalcular */
    $(document).on("click", "#btnCalcular", function(evt)
    {
        var kgAlimentoHomem = (pesoMedioHomem * qtdHomem);
        var kgAlimentoMulher = (pesoMedioMulher * qtdMulher);
        var kgAlimentoCrianca = (pesoMedioCrianca * qtdCrianca);
              
        var qtdKgCarneHomem = (kgAlimentoHomem * percentCarne)/100;
        var qtdKgCarneMulher = (kgAlimentoMulher * percentCarne)/100;
        var qtdKgCarneCrianca = (kgAlimentoCrianca * percentCarne)/100;
        
        var qtdTotalCarne = qtdKgCarneHomem + qtdKgCarneMulher + qtdKgCarneCrianca;
            
        var qtdKgSaladaHomem = (kgAlimentoHomem * percentSalada)/100;
        var qtdKgSaladaMulher = (kgAlimentoMulher * percentSalada)/100;
        var qtdKgSaladaCrianca = (kgAlimentoCrianca * percentSalada)/100;
        
        var qtdTotalSalada = qtdKgSaladaHomem + qtdKgSaladaMulher + qtdKgSaladaCrianca;
        
        var litrosHomem = 0.5 * $("#tempoFesta").val() * qtdHomem;
        var litrosMulher = 0.3 * $("#tempoFesta").val() * qtdMulher;
        var litrosCriança = 0.2 * $("#tempoFesta").val() * qtdCrianca;
        
        var totalLitrosBebidas = (litrosHomem + litrosMulher + litrosCriança);
                        
        $("#kgTotalCarne").val(qtdTotalCarne/1000);
        $("#kgTotalSalada").val(qtdTotalSalada/1000);
        $("#litrosCerveja").val((totalLitrosBebidas*percentCerveja)/100);
        $("#litrosRefri").val((totalLitrosBebidas*percentRefri)/100);
            
        return false;
    });
    
        /* button  #btnVoltarAlimentos */
    $(document).on("click", "#btnVoltarAlimentos", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#calcular"); 
         return false;
    });
    
        /* button  #btnVoltarBebida */
    $(document).on("click", "#btnVoltarBebida", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#calcular"); 
         return false;
    });
    
        /* button  #btnVoltarParticipantes */
    $(document).on("click", "#btnVoltarParticipantes", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#calcular"); 
         return false;
    });
    
        /* button  #btnVoltarPesoPessoa */
    $(document).on("click", "#btnVoltarPesoPessoa", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#calcular"); 
         return false;
    });
    
    }
    document.addEventListener("app.Ready", register_event_handlers, false);
        
})();


function editTrabalho(codtra) {
    
    db.findTrabalhoById(codtra, function(trabalho){
        if (trabalho != null) {
            $("#txtCodigoTrabalho").val(trabalho.codtra),
            $("#txtAddNomeTrabalho").val(trabalho.nomtra),
            $("#txtAddNomeCurso").val(trabalho.nomcur)
        }
        
    });
    activate_subpage("#sbtrabalho");
    return false;
}

function deleteTrabalho(codtra) {
    db.deleteTrabalho(JSON.stringify({
        "codtra": codtra
    }), function (status) {
        if (status == true) {
            // removendo elementos
            var item = document.getElementById(codtra);
            item.parentNode.removeChild(item);
        }
    });
}

function saveAssinatura() {
    //$("#img").html(padAssinatura.toDataURL());
    activate_subpage("#page_55_16");
    return false;
}

function clearAssinatura() {
    padAssinatura.clear();
}

function onErrorFoto(erroFoto) {
    alert("Erro na captura da foto!" + erroFoto);
}

function onSuccessFoto(foto) {
    // exibindo a foto
    $("#imgaluno").attr("src",
        "data:image/jpeg;base64," +
        foto);
}

function addAluno(){
    activate_subpage("#sbalunos");
    return false;
}

function addTrabalho(){
    activate_subpage("#sbtrabalho");
    return false;
}
